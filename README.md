


### Install the yarn package manager

Install the `yarn` package manager as a global installation and use `yarn` instead of `npm`as`yarn` is fast and works really well:

```
npm install yarn -g
```

If you're running on Linux/macOS, add a sudo in front of the command:

```
sudo npm install yarn -g
```
> Note that: Using `yarn` is optional.

### .env Configuration

Create a .env file in the server root directory and copy the following information:

```

ACCESS_TOKEN_SECRET = jsfgfjguwrg8783wgbjs849h2fu3cnsvh8wyr8fhwfvi2g225
REFRESH_TOKEN_SECRET = 825y8i3hnfjmsbv7gwajbl7fobqrjfvbs7gbfj2q3bgh8f42
AUTH_DB=auth_db
HOTEL_DB=hotels
DB_USER_NAME=(your username)
USER_PASS=(your password)
DB_HOST=localhost
DB=mysql

```
To complete database configuration, read the documentation under the server/config folder.

*NB: You can also see the data in the client folder by running the following command: npm run server
Then hit the following url: http://localhost/3001/hotels

# Run the application

Before run the application, make sure you installed dependencies for both client and server. Clone the repo and then cd server npm install cd client npm install

After you installed the dependencies, you ought to run both server and client at the same time:

in a new terminal: cd server npm start

in a new terminal: cd client npm start


## Tech Stack

**Client:** React, MaterialUI

**Server:** Node, Express, Msql/Sequelize

 


## Usage/Examples

```javascript
import Component from 'my-project'

function App() {
  return <Component />
}
```

## Running Tests

To run tests, run the following command

```bash
  npm run test
```

