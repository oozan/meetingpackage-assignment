import { Sequelize } from "sequelize";
import {auth_db} from "../config/Database.js";

const { DataTypes } = Sequelize;

// Creating the User schema (Schema defines the structure of the data)
const Users = auth_db.define('users',{
    name:{
        type: DataTypes.STRING
    },
    email:{
        type: DataTypes.STRING
    },
    password:{
        type: DataTypes.STRING
    },
    refresh_token:{
        type: DataTypes.TEXT
    }
},{
    freezeTableName:true
});

(async () => {
    await auth_db.sync();
})();

export default Users;