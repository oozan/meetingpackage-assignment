import { Sequelize } from "sequelize";
import {hotel_db} from "../config/Database.js";

const { DataTypes } = Sequelize;

// Creating the Firman Hotels schema (Schema defines the structure of the data)
const Hotels = hotel_db.define('hotels_infos',{
    hotel_name:{
        type: DataTypes.STRING
    },
    channel:{
        type: DataTypes.STRING
    },
    country_code:{
        type: DataTypes.STRING
    }
},{
    timestamps: false
});

(async () => {
    await hotel_db.sync();
})();


export default Hotels;