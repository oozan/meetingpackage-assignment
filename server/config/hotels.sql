/* CREATE TABLE */
CREATE TABLE IF NOT EXISTS hotels_infos(
  id INT,
  hotel_name VARCHAR(100), 
  channel VARCHAR(100), 
  country_code VARCHAR(100)
);
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 0,
    'Hilton Berlin City East', 
    'hotels.com', 'DE'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 1,
    'Hilton Berlin City West', 
    'hotels.com', 'DE'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 2,
    'Arctic Tree House Hotel ', 'visitrovaniemi.fi', 
    'FI'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 3,
    'Fraser Place Robertson Walk Singapore', 
    'booking.com', 'SG'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 4,
    'Rydges Sydney Airport Hotel ', 
    'booking.com', 'AU'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 5,
    'Romance Istanbul Hotel', 'tripadvisor.com', 
    'TR'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 6,
    'Hilton Niagara Falls Fallsview', 
    'expedia.com', 'CA'
  );
/* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 7,
    'Hotel Brosundet', 'trivago.com', 
    'NO'
  );
  /* INSERT QUERY */
INSERT INTO hotels_infos(
  id, hotel_name, channel, country_code
) 
VALUES 
  ( 8,
    'Fairmont Royal York', 'tripsinsider.com', 
    'CA'
  );
 