## Setup your database

To be able to start database locally and properly you need two DB connections. The first is for the authentication and the second is for the hotels.

First install mysql(preferred) on your local machine:

For ubuntu users: https://linuxhint.com/install-mysql-linux-mint-ubuntu/

For mac users: https://www.positronx.io/how-to-install-mysql-on-mac-configure-mysql-in-terminal/

For windows users: https://phoenixnap.com/kb/install-mysql-on-windows

After you installed mysql to your computer, open up a new terminal:

For ubuntu: https://www.wikihow.com/Open-a-Terminal-Window-in-Ubuntu

For mac: https://support.apple.com/guide/terminal/keyboard-shortcuts-trmlshtcts/mac

For windows: https://www.wikihow.com/Open-Terminal-in-Windows


And when you are on the terminal in the /server folder login with your username and password: 

```
cd config
```

```
mysql -u root -p
```
sudo may required for linux users.


Then create a database for the authentication:

```
CREATE DATABASE auth_db;
```

Then create a database for the hotels:


```
CREATE DATABASE hotels;
```

Close the terminal and open up a new terminal in the /config directory again.

Lastly, import the hotels.sql to hotels database by the following command:

```
mysql -u username -p  hotels < hotels.sql
```
sudo may required for linux users and for the windows users you may need to specify the hotels.sql source path e.g:

```
mysql -u username -p  hotels < file_name\another_file\etc\hotels.sql
```


To be able to run the Database.js properly you will need to add local environment variables to your .env file which is stated in the main README.md file.

After you follow and finish the main readme file, finally here you can run your app.


If you want to skip this entire database process, you can run 


```
If you get   'sqlMessage: 'Client does not support authentication protocol requested by server; consider upgrading MySQL client',' error message on the terminal,

follow these steps:

Open terminal and login:

mysql -u username -p

(then type your password)

After that:
alter user 'user_name'@'localhost' identified with mysql_native_password by 'password';
```