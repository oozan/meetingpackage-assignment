import { Sequelize } from "sequelize";
import {} from "dotenv/config";

// Database connection for user authentication.
export const auth_db = new Sequelize(
  process.env.AUTH_DB,
  process.env.DB_USER_NAME,
  process.env.USER_PASS,
  {
    host: "localhost",
    dialect: "mysql",
  }
);


// Database connection for hotels
export const hotel_db = new Sequelize(
    process.env.HOTEL_DB,
    process.env.DB_USER_NAME,
    process.env.USER_PASS,
    {
      host: "localhost",
      dialect: "mysql",
    }
  );
  