import hotels from "../models/HotelsModel.js";



// Get all hotels information from database.
export const getHotels = async (req, res) => {
  try {
    const hotel_info = await hotels.findAll({
      attributes: ["id", "hotel_name", "channel", "country_code"],
    });
    res.status(200).send({
        status: 200,
        message: 'Data fetched Successfully',
        data: hotel_info
    })
  } catch (error) {
    return res.status(400).send({
        message: 'Unable to fetch data: ' + error.message,
        error: error,
        status: 400
    })
  }
};


// Create hotel and save to database.
export const createHotel = async (req, res) => {
  const {id, hotel_name, channel, country_code} = req.body 
  try {
      const create_hotel = await hotels.create({
          id: id,
          hotel_name: hotel_name,
          channel: channel,
          country_code: country_code,
      });
      res.status(200).send({
          status: 200,
          message: 'Data Save Successfully',
          data: create_hotel
      });
  }
  catch (error) {
      return res.status(400).send({
          message: 'Unable to insert data: ' + error.message,
          errors: error,
          status: 400
      });
  }
}

// Delete hotel from the database.
export const deleteHotel = async (req, res) => {
  try {
    const delete_hotel = await hotels.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).send({
        status: 200,
        message: 'Data Deleted Successfully',
        data: delete_hotel
    });
}
catch (error) {
    return res.status(400).send({
        message: 'Unable to delete data: ' + error.message,
        errors: error,
        status: 400
    });
}
};