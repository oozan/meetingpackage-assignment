import {render, screen, fireEvent} from '@testing-library/react'
import React from 'react'

const Button = ({onClick, children}) => (
  <button onClick={onClick}>{children}</button>
)

test('calls onClick prop when clicked', () => {
  const enableClick = jest.fn()
  render(<Button onClick={enableClick}>Submit</Button>)
  fireEvent.click(screen.getByText(/submit/i))
  expect(enableClick).toHaveBeenCalledTimes(1)
})