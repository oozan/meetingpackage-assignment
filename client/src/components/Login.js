import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msg, setMsg] = useState("");
  const history = useHistory();

  const Auth = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/login", {
        email: email,
        password: password,
      });
      history.push("/hotels");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };

  return (
        <div className="login-main">
          <div className="container">
            <form onSubmit={Auth}>
              <label>
                Login
              </label>
              <input
                type="text"
                className="input"
                required
                placeholder="Username"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <input
                type="password"
                className="input"
                required
                placeholder="******"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <button>Login</button>
              <a className="form-link" href="/">
                <p className="link-text">Register</p>
              </a>
              <p className="msg">{msg}</p>
            </form>
          </div>
        </div>
  );
};

export default Login;
