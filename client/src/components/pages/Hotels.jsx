import React from "react";
import HotelTable from "../HotelTable";
import Navbar from "../Navbar";
import Footer from "../Footer";
import AddModal from "../AddModal";

export default function Hotels() {


  return (
    <>
      <Navbar />

      <HotelTable/>
      <AddModal />
      <Footer />
    </>
  );
}
