import * as React from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import axios from 'axios';

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 6,
};

export default function BasicModal() {
  const [open, setOpen] = React.useState(false);
  const [hotel, setHotel] = React.useState("");
  const [channel, setChannel] = React.useState("");
  const [country, setCountry] = React.useState("");
  const [id, setId] = React.useState(0);
  const [msg, setMsg] = React.useState("");


  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);


  const AddHotel = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/hotel", {
        id: id,
        hotel_name: hotel,
        channel: channel,
        country_code: country
      });
      setOpen(false)
      window.location.reload();
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  

  return (
    <>
      <button onClick={handleOpen}>Add Hotel</button>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <form onSubmit={AddHotel}>
          <input
              type="hidden"
              className="input"
              hidden
              value={id}
              onChange={(e) => setId(e.target.value)}
              placeholder="Hotel Name"
            />
            <input
              type="text"
              className="input"
              required
              value={hotel}
              onChange={(e) => setHotel(e.target.value)}
              placeholder="Hotel Name"
            />
            <input
              type="text"
              className="input"
              required
              value={channel}
              onChange={(e) => setChannel(e.target.value)}
              placeholder="Channel Name"
            />
            <input
              type="text"
              className="input"
              required
              value={country}
              onChange={(e) => setCountry(e.target.value)}
              placeholder="Country Code"
            />
            <button>Add</button>
            <p className="msg">{msg}</p>
          </form>
        </Box>
      </Modal>
    </>
  );
}
