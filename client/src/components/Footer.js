import * as React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Typography from '@mui/material/Typography';



const useStyles = makeStyles({
  footer: {
    textAlign: "center",
    color: '#fcfcfc',
    marginTop: '2rem'
  }
});

export default function ActionAreaCard() {
  const classes = useStyles();
  return (
      <footer>
          <Typography className={classes.footer}>
            All rights are reserved © 
          </Typography>
    </footer>
  );
}
