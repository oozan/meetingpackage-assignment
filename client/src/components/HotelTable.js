import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TinyFlag from "tiny-flag-react";
import axios from "axios";

const useStyles = makeStyles({
  flag: {
    textAlign: "center",
    cursor: "pointer",
  },
  radius: {
    borderRadius: "0px",
  },
  table: {
    minWidth: 400,
  },
  transparent: {
    opacity: 0.4,
  },
});

function flagURL(code) {
  return `https://cdn.jsdelivr.net/npm/react-flagkit@1.0.2/img/SVG/${code}.svg`;
}

export default function HotelTable() {
  const classes = useStyles();
  const [hotels, setHotels] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:5000/hotels")
      .then((res) => {
        setHotels(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const handleDelete = async (id) => {
    try {
      await axios.delete(`http://localhost:5000/hotel/${id}`).then((res) => {
        const del = hotels.filter((hotel) => id !== hotel.id);
        setHotels(del);
      });
    } catch (error) {
      if (error.response) {
        console.log(error);
      }
    }
  };

  return (
    <TableContainer className={classes.radius} component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Hotel Name</TableCell>
            <TableCell align="left">Channel</TableCell>
            <TableCell align="right">Country</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {hotels && hotels.map((hotel) => {
              return (
              <>
                <TableRow key={hotel.id}>
                  <TableCell align="left">{hotel.hotel_name}</TableCell>
                  <TableCell align="left">{hotel.channel}</TableCell>
                  <TableCell className={classes.flag}>
                    <Button onClick={() => handleDelete(hotel.id)}>
                      <TinyFlag
                        country={hotel.country_code}
                        alt={hotel.country_code}
                        backImageURL={flagURL(hotel.country_code)}
                      />
                    </Button>
                  </TableCell>
                </TableRow>
              </>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
