import React, { useState } from 'react'
import axios from "axios";
import { useHistory } from "react-router-dom";
 
const Register = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confPassword, setConfPassword] = useState('');
    const [msg, setMsg] = useState('');
    const history = useHistory();
 
    const Register = async (e) => {
        e.preventDefault();
        try {
            await axios.post('http://localhost:5000/users', {
                name: name,
                email: email,
                password: password,
                confPassword: confPassword
            });
            history.push("/login");
        } catch (error) {
            if (error.response) {
                setMsg(error.response.data.msg);
            }
        }
    }
 
    return (
        <div className="register-main">  	
			<div className="container">
				<form onSubmit={Register}>
					<label>Sign up</label>
                     <input type="text" className="input" placeholder="Name (Optinal)" value={name} onChange={(e) => setName(e.target.value)} />
					 <input type="text" className="input" required placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
                     <input type="password" className="input" required placeholder="******" value={password} onChange={(e) => setPassword(e.target.value)} />
                     <input type="password" className="input" required placeholder="******" value={confPassword} onChange={(e) => setConfPassword(e.target.value)} />
					<button>Sign up</button>
                    <p className="msg">{msg}</p>
				</form>
                <a href="/login"><p className="link-text">Already have an account?</p></a>
			</div>
            
	</div>
    )
}
 
export default Register
