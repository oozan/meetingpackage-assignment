import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import Login from "./components/Login";
import Register from "./components/Register";
import { LinearProgress } from "@mui/material";
import Hotels from "./components/pages/Hotels";

function App() {
  return (
    <Router>
      <Suspense fallback={<LinearProgress />}>
        <Switch>
          <Route path="/hotels">
            <Hotels />
          </Route>
          <Route path="/" exact>
            <Register />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
